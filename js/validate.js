function validate() {
    return checkName();

}


function checkName(){
var name = document.getElementById("name");
    
    if (name.value==""){
        document.getElementById("nameMissing").style.visibility = "visible";
        name.focus();    
        return false;
    }else{
		document.getElementById("nameMissing").style.visibility = "hidden";
		}
    return checklastName();

}

function checklastName(){
    var lastname = document.getElementById("lastname");
    
    if (lastname.value==""){
        document.getElementById("surnameMissing").style.visibility = "visible";
        lastname.focus();
        return false;
    }else{
		document.getElementById("surnameMissing").style.visibility = "hidden";
		}
    return checkPhone();

}

function checkPhone(){
    var phone = document.getElementById("phone");
    var v = /^$/.test(phone.value);
    
    if(v){//if v is true, it means that the regEx matched.
        document.getElementById("phoneMissing").style.visibility = "visible";
		phone.focus();
        return false;
    }else{
		document.getElementById("phoneMissing").style.visibility = "hidden";
		}
    return checkSaddress();
}


function checkSaddress(){
    var address = document.getElementById("saddress");
    var e = /^$/.test(address.value);
    if(e){
		document.getElementById("saddressMissing").style.visibility = "visible";
		address.focus();
        return false;
    }else{
		document.getElementById("saddressMissing").style.visibility = "hidden";
		}
    return checkState(); 
}

function checkState(){
    var state = document.getElementById("state");
    if(state.selectedIndex==0){
		document.getElementById("stateMissing").style.visibility = "visible";
		state.focus();
        return false;
    }else{
		document.getElementById("stateMissing").style.visibility = "hidden";
		}
    return checkGender();

}

function checkGender(){
    var male = document.getElementById("male");
    var female = document.getElementById("female");
    if(male.checked){
		document.getElementById("genderMissing").style.visibility = "hidden";
        return checkVegetarian();
    }else if(female.checked){
		document.getElementById("genderMissing").style.visibility = "hidden";
        return checkVegetarian();
    }
	document.getElementById("genderMissing").style.visibility = "visible";
    return false;
}


function checkVegetarian(){
    var vegetarian =  document.getElementById("vegetarian");
    if(vegetarian.checked){
		document.getElementById("vegetarianMissing").style.visibility = "hidden";
        return checkUsername();
    }
	document.getElementById("vegetarianMissing").style.visibility = "visible";
    return false;
}

function checkUsername(){
    var username= document.getElementById("username");
    //if the value of username matches the pattern, test() returns TRUE
    var empt = /^$/.test(username.value);
    
    if(empt){
		document.getElementById("usernameMissing").style.visibility = "visible";
		username.focus();
        return false;
    }else{
		document.getElementById("usernameMissing").style.visibility = "hidden";
		}
    return checkPassword();
}

function checkPassword(){
    var passw1 = document.getElementById("password");
    var passw2 = document.getElementById("checkPassword");
    
    if (/^$/.test(passw1.value )){
		document.getElementById("passwordMissing").style.visibility = "visible";
		document.getElementById("doNotMatch").style.visibility = "hidden";
		passw1.focus();
        return false;
    }else if(/^$/.test(passw2.value )){
		document.getElementById("genderMissing").style.visibility = "visible"; 
		document.getElementById("doNotMatch").style.visibility = "hidden";
		passw2.focus();
        return false;
    }else if (passw1.value != passw2.value){
		document.getElementById("passwordMissing").style.visibility = "hidden";
		document.getElementById("doNotMatch").style.visibility = "Visible";
        return false;
    }   
    return true;

}

/*
Before every return false; we have to change the state of some 
span that alerts the user what was done wrong, instead of using alerts.
*/



/*
The following funciton should receive a parameter
as the Id of the span! that would be sweet
*/

function hideSpan(){

    document.getElementById("surnameMissing").style.visibility="hidden";
}
