//The following code will be executed when the DOM is ready

$(function(){ 
	$('#menu-text').click(function(){
		$("#link-list").slideToggle();
	});

	/*
	This line checks the width of the window to 
	remove the style (display:none) which results 
	from the last event.
	*/

	$(window).resize(function() {
		if($(window).width()>730){
				$("#link-list").removeAttr('style');
			}
	});

});
/*
Script that loads the map. There is the need of a listener
when the content of the page is being loaded. This listener checks 
when the content is ready, it triggers the function initMap 
which initializes the Map.
We need an object (mapOptions) that contains different parameters
that are going to be part of the map when it renders.
*/
google.maps.event.addDomListener(window, "load", initMap);

function initMap(){
	var mapOptions = { 
		center: new google.maps.LatLng(-27.470933,153.024629),
		zoom: 12,
		mapTypeId: google.maps.MapTypeId.ROADMAP
		};
	//This is where the map is created based on the options passed by mapOptions.			
	new google.maps.Map(document.getElementById('map'), mapOptions);
};


/*
The following function's purpose is to submit the form with an id as a post ,
it makes the stars (images) a button, I wanted to use a javascript function 
to add events (onClick) to simple images, I could have surrounded them with anchor tags
and send the info in a similar way. I could not use AJAX, and create a form for each star
was not that clean.
*/
  
function send(stars){
	window.location = "results.html?stars="+stars;
}
		

function getLocation(lat, long) {
	if (navigator.geolocation) {
		showPosition(lat, long);
	} else {
		document.getElementById("status").innerHTML="Geolocation is not supported by this browser.";
	}
}
function showPosition(lat, long) {
	// display on a map
	var latlon = lat + "," + long;
	var img_url = "http://maps.googleapis.com/maps/api/staticmap?center="+latlon+"&zoom=14&size=400x300&sensor=false";
	document.getElementById("mapholder").innerHTML = "<img src='"+img_url+"'>";
	
}
function showError(error) {
	var msg = "";
	switch(error.code) {
		case error.PERMISSION_DENIED:
			msg = "User denied the request for Geolocation."
			break;
		case error.POSITION_UNAVAILABLE:
			msg = "Location information is unavailable."
			break;
		case error.TIMEOUT:
			msg = "The request to get user location timed out."
			break;
		case error.UNKNOWN_ERROR:
			msg = "An unknown error occurred."
			break;
	}
	document.getElementById("status").innerHTML = msg;
}

		